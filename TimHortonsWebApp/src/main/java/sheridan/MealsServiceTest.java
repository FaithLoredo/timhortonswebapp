package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {
	
	@Test
	public final void testDrinksRegular()
	{
		MealsService m = new MealsService();
		assertTrue("Missing Drinks",m.getAvailableMealTypes(MealType.DRINKS)!=null);
	}
	
	@Test
	public final void testDrinksException()
	{
		MealsService m = new MealsService();
		String toTest=m.getAvailableMealTypes(null).get(0);
		assertTrue("Null unhandled",toTest=="No Brand Available");
	}
	
	@Test
	public final void testDrinksBoundaryIn()
	{
		MealsService m = new MealsService();
		List<String> toTest=m.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("No Drinks in menu", toTest.size()>3);
	}
	
	@Test
	public final void testDrinksBoundaryOut()
	{
		MealsService m = new MealsService();
		List<String> toTest=m.getAvailableMealTypes(null);
		assertFalse("No Drinks in menu", toTest.size()<1);
	}
}
